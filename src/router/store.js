export default [
  {
    path: '/v-about',
    name: 'v-about',
    component: () => import(/* webpackChunkName: "about" */ '../views/store/About.vue')
  },
  {
    path: '/v-news',
    name: 'v-news',
    component: () => import('../views/store/News.vue')
  },
  {
    path: '/v-news/:id',
    name: 'v-single-news',
    component: () => import('../views/store/SingleNews.vue')
  }
]
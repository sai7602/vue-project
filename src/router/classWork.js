
export default [
  {
    name: 'home-page',
    path: '/home-page',
    component: () => import('@/views/classWork/HomePage.vue')
  },
  {
    name: 'list',
    path: '/list',
    component: () => import('@/views/classWork/Artists.vue')
  },
  {
    name: 'list-item',
    path: '/list/:item',
    component: () => import('@/views/classWork/Albums.vue'),
    children: [
      {
        name: 'composition',
        path: ':comp',
        component: () => import('@/views/classWork/Composition.vue')
      }
    ]
  }
]
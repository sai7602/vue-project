import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from '@/views/Home.vue'

import VueRouters from './routers'
import Vuex from './store'

import classWork from './classWork'

import p404 from '@/views/404'

Vue.use(VueRouter)

const routes = [
  ...VueRouters,
  ...Vuex,
  ...classWork,
  {
    path: '/',
    name: 'home',
    component: Home,
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/router/About.vue')
  },

  /*  '*' звездочка - принимает лбой роут не подошедший к другим.  */
  {
    path: '*',
    component: p404
  },

  /* Звездочку также можна использовать для динамических путей */
  {
    path: '/post-*',
    component: Home
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router

export default {
  methods: {
    getImg(news) {
      const defImg = 'https://cdn.ymaws.com/www.itsmfusa.org/resource/resmgr/images/more_images/news-3.jpg'
      return news.img ? news.img : defImg
    }
  }
}